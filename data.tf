

//THis will fetch the information of terrfafrom cpc VPC  


data "terraform_remote_state" "vpc" {
  backend = "s3"
  config = {
    bucket = var.BUCKET
    key    = "student/vpc/${var.ENV}/terraform.tfstate"
    region = "us-east-1"
  }
}
